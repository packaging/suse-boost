#!/bin/bash

set -e

sudo zypper search boost icinga

icinga-build-rpm-install \
  libboost_system1_69_0-icinga \
  libboost_filesystem1_69_0-icinga-devel \
  libboost_thread1_69_0-icinga-devel

set -x

find /usr/lib64/icinga-boost
ls -al /usr/include/icinga-boost
